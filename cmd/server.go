/*
Copyright © 2020 Sung Kang <skang0601@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/skang0601/franklin-slackbot/internal"
	"net/http"
)


// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: server,
}

func pong(w http.ResponseWriter, req *http.Request) {
	if _, err := fmt.Fprint(w, "pong"); err != nil {
		logrus.Fatal("/pong errored with %s", err)
	} else {
		w.WriteHeader(200)
		_, _ = fmt.Fprintf(w, "pong")
	}


}

func slack(w http.ResponseWriter, req *http.Request) {
	logrus.Info(req.Body)
}

func franklinActiveOrders(w http.ResponseWriter, req *http.Request) {
	activeOrders, err := internal.GetActiveGroupOrders(internal.TableName)

	if err != nil {
		logrus.Fatal("Error querying for active group orders", err)
	}


	b, err := json.Marshal(internal.GroupOrderOptionBlocks(activeOrders))

	if err != nil {
		logrus.Fatal("Error marshalling JSON", err)
	}

	_, err = w.Write(b); if err != nil {
		logrus.Fatal("Error responding to request", err)
	}

}

func server(cmd *cobra.Command, args []string) {
	logrus.Info(cmd.Flag("port"))
	logrus.Info(args)


	/*
	* Initialize the database backend (DynamoDB)
	*/

	/*
	* Register Routes
	*/
	http.HandleFunc("/ping", pong)
	http.HandleFunc("/slack", slack)
	http.HandleFunc("/slack/franklins/activeOrders", franklinActiveOrders)

	/*Start our server*/
	logrus.Info("Starting server on port ", viper.GetInt("port"))
	if err := http.ListenAndServe(fmt.Sprintf("localhost:%d", viper.GetInt("port")), nil); err != nil {
		logrus.Fatal("Server error ", err)
	}
}

func init() {
	rootCmd.AddCommand(serverCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	serverCmd.PersistentFlags().Int("port", 8080, "Server port")

	if err := viper.BindPFlags(serverCmd.PersistentFlags()); err != nil {
		logrus.Fatal("Unable to bind persistent flags to Viper", err)
	}

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serverCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
