/*
Copyright © 2020 Sung Kang <skang0601@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/skang0601/franklin-slackbot/internal"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/cobra"
)

// franklinCmd represents the franklin command
var franklinCmd = &cobra.Command{
	Use:   "franklin",
	Short: "Used to list/modify/create Franklins group orders loocally",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("franklins called")
	},
}

var menuCmd = &cobra.Command{
	Use:   "menu",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		var franklinMenu internal.FranklinMenu

		jsonFile, err := ioutil.ReadFile("./assets/franklin.json")

		if err != nil {
			log.Fatal("Error opening menu json file")
			os.Exit(1)
		}

		err = json.Unmarshal(jsonFile, &franklinMenu)

		if err != nil {
			log.Fatal("Error parsing menu json file", err)
			os.Exit(1)
		}

		slackModal := internal.GetSlackMenuModal(franklinMenu)

		b, err := json.Marshal(slackModal)

		if err != nil {
			os.Exit(1)
		}



		logrus.Info(string(b))

	},
}

var orderCmd = &cobra.Command{
	Use:   "order",
	Short: "A brief description of your command",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {


		fmt.Println("Implement me!")
	},
}

var orderAddCmd = &cobra.Command{
	Use:   "create",
	Short: "Creates a new group order",
	Long: `A longer description that spans multiple lines and likely contains examples
and usage of using your command. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {


		fmt.Println("Implement me!")
	},
}



func init() {
	rootCmd.AddCommand(franklinCmd)
	franklinCmd.AddCommand(menuCmd)
	franklinCmd.AddCommand(orderCmd)

	orderCmd.AddCommand(orderAddCmd)


	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// franklinCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// franklinCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
