module gitlab.com/skang0601/franklin-slackbot

go 1.13

require (
	github.com/aws/aws-sdk-go v1.29.29
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/mitchellh/mapstructure v1.2.2 // indirect
	github.com/pelletier/go-toml v1.6.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/slack-go/slack v0.6.3
	github.com/spf13/afero v1.2.2 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	github.com/spf13/cobra v0.0.6
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.6.2
	golang.org/x/sys v0.0.0-20200320181252-af34d8274f85 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/ini.v1 v1.55.0 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
