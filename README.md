Franklin Bot
=============

A slack bot/CLI to help create and manage Franklin group orders at the office.

TODO:
 - Finish defining out interaction between application and dynamodb or other KV store
 - Finish creating Slack interaction workflow
 - Add gitlab pipeline for auto build/deployment
 - Write better documents for cobra cli help menus
 - Write Dockerfile
 - Create KMS keys for Slack API key encryption
 - Tests
 
Kubernetes as a runtime
 - Find a container registry to run application on
 - Find a Kubernetes cluster to run application on
 - Finish writing Kubernetes manifests for deployment

AWS Lambda as a runtime
 - Find a AWS account to run lambda in (if no kubernetes cluster available)
 - Write Terraform config for AWS Lambda and API gateway
 

Installations
===
```bash
brew install make, golang, docker, sops, kubectl, kustomize, terraform, terragrunt
```

Build
======
```bash


# Make build (Coming soon)

go build main.go

# To run locally

aws-vault exec [PROFILE_NAME] -- ./main 
```



Deployment
==========
```bash

kubectl apply -k kubernetes/overlays/cx-nonprod/kustomization.yaml
```