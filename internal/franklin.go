/*
Copyright © 2020 Sung Kang <skang0601@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package internal

import (
	"fmt"
	"time"
)

const (
	TableName = "franklin"
	awsAccountId = "heb-eng-sandbox"
	displayOptionSize = 10
)

func (menu FranklinMenu) name() string {
	return "Franklin BBQ"
}

func (menu FranklinMenu) menuItems() []MenuItem {
	menuItems := make([]MenuItem, len(menu.Items))

	for i, item := range menu.Items {
		menuItems[i] = MenuItem(item)
	}

	return menuItems
}

func (item FranklinMenuItem) id() int {
	return item.Id
}

func (item FranklinMenuItem) name() string {
	return item.Name
}

func (item FranklinMenuItem) options() []Option {
	var itemOptions []Option

	for i := 1; i <= displayOptionSize; i++ {
		option := float32(i) * item.Step
		itemOptions = append(itemOptions, Option{
			value: fmt.Sprintf("%.2f", option),
			display: fmt.Sprintf("%.2f %s", option, item.PriceUnit),
		})
	}
	return itemOptions
}

func (item FranklinMenuItem) step() float32 {
	return item.Step
}

func (item FranklinMenuItem) priceUnit() string {
	return item.PriceUnit
}

func (item FranklinMenuItem) menuItem() MenuItem {
	return MenuItem(item)
}



func (order FranklinGroupOrder) name() string {
	return fmt.Sprintf("%s Group Order - %s", order.Name, order.orderDate().Format("%B %d, %Y"))
}

func (order FranklinGroupOrder) orderDate() time.Time {
	return order.OrderDate
}

func (order FranklinGroupOrder) isClosed() bool {
	/*
	A group order for Franklin is close when the we've hit one of the following conditions
	1) Order date is today
	2) We've reached the maximal weight limit for the order
	*/
	now := time.Now()

	if order.OrderDate.After(now) || order.CurrentWeight >= order.Limit {
		return false
	} else {
		return true
	}
}

func (order FranklinGroupOrder) addOrder(item GroupOrderItem) (bool, error) {
	return false, nil
}

func (order FranklinGroupOrder) removeOrder(item GroupOrderItem) (bool, error) {
	return false, nil
}

func (order FranklinGroupOrder) updateOrder(item GroupOrderItem) (bool, error) {
	return false, nil
}