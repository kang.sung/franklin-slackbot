/*
Copyright © 2020 Sung Kang <skang0601@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

/*
 This package attempts to encapsulate all the Slack related Block elements (modals,selections,etc.) that the bot
 will generate.

 TBD: Need to abstract/clean this up some more to be resuable for a general menu-based slack bot organizer.
*/

package internal

import (
	"fmt"
	"github.com/slack-go/slack"
)

func GetSlackMenuModal(menu Menu) *slack.ModalViewRequest {
	return &slack.ModalViewRequest{
		Type:            "modal",
		Title:           slack.NewTextBlockObject(slack.PlainTextType, fmt.Sprintf("%s Menu Order", menu.name()), false, false),
		Blocks:          menuSlackBlocks(menu),
		Close:           slack.NewTextBlockObject(slack.PlainTextType, "Close", false, false),
		Submit:          slack.NewTextBlockObject(slack.PlainTextType, "Submit", false, false),
		PrivateMetadata: "",
		CallbackID:      "",
		ClearOnClose:    false,
		NotifyOnClose:   false,
		ExternalID:      "",
	}
}

func menuSlackBlocks(menu Menu) slack.Blocks {
	var blocks []slack.Block

	for _, item := range menu.menuItems() {
		var sectionBlock = slack.NewSectionBlock(
			slack.NewTextBlockObject(slack.MarkdownType, item.name(), false, false),
			nil,
			slack.NewAccessory(
				slack.NewOptionsSelectBlockElement(
					slack.OptTypeStatic,
					slack.NewTextBlockObject(
						slack.PlainTextType,
						fmt.Sprintf("Choose your choice in %s", item.priceUnit()),
						false,
						false,
					),
					fmt.Sprintf("%d.action", item.id()),
					menuSlackOptions(item)...,
				),
			),
			func(block *slack.SectionBlock) {},
			)


		blocks = append(blocks, sectionBlock)
	}
	return slack.Blocks{BlockSet: blocks}
}

func menuSlackOptions(menu MenuItem) []*slack.OptionBlockObject {
	var optionBlocks []*slack.OptionBlockObject

	for _, option := range menu.options() {
		option := slack.NewOptionBlockObject(
			option.value,
			slack.NewTextBlockObject(slack.PlainTextType, option.display, false, false),
		)

		optionBlocks = append(optionBlocks, option)
	}

	return optionBlocks
}

func GetSlackGroupOrderModal(order GroupOrder) *slack.ModalViewRequest {
	return &slack.ModalViewRequest{
		Type:            "modal",
		Title:           slack.NewTextBlockObject(slack.PlainTextType, order.name(), false, false),
		Blocks:          groupOrderSlackBlocks(order),
		Close:           slack.NewTextBlockObject(slack.PlainTextType, "Close", false, false),
		Submit:          slack.NewTextBlockObject(slack.PlainTextType, "Submit", false, false),
		PrivateMetadata: "",
		CallbackID:      "",
		ClearOnClose:    false,
		NotifyOnClose:   false,
		ExternalID:      "",
	}
}

func groupOrderSlackBlocks(order GroupOrder) slack.Blocks {
	var blocks []slack.Block

	var sectionBlock = slack.NewSectionBlock(
		slack.NewTextBlockObject(slack.MarkdownType, "Select an upcoming group order :party_wizard: !", true, false),
		nil,
		slack.NewAccessory(
			slack.NewOptionsSelectBlockElement(
				slack.OptTypeExternal,
				slack.NewTextBlockObject(
					slack.PlainTextType,
					fmt.Sprintf("Choose the group order to participate in"),
					false,
					false,
				),
				fmt.Sprintf("groupOrder.action"),
			),
		),
		func(block *slack.SectionBlock) {},
	)

	blocks = append(blocks, sectionBlock)

	return slack.Blocks{BlockSet:blocks}
}

func GroupOrderOptionBlocks(orders []GroupOrder) []*slack.OptionBlockObject {
	var optionBlocks []*slack.OptionBlockObject
	for _, order := range orders {
		option := slack.NewOptionBlockObject(
			order.id(),
			slack.NewTextBlockObject(slack.PlainTextType, order.orderDate().Format("%B %d, %Y"), false, false),
		)

		optionBlocks = append(optionBlocks, option)
	}

	return optionBlocks
}