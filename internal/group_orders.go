package internal

import "time"

type GroupOrder interface {
	id() string
	name() string
	orderDate() time.Time
	isClosed() bool
	addOrder(order GroupOrderItem) (bool, error)
	removeOrder(order GroupOrderItem) (bool, error)
	updateOrder(order GroupOrderItem) (bool, error)
}

type GroupOrderItem interface {
	getId() int
	getUser() string
	getOrderTotal() float32
}

func GetActiveGroupOrders(tableName string) ([]GroupOrder, error) {

}