package internal

import "time"

type FranklinGroupOrder struct {
	Id int
	Name string
	Members []string
	Orders []FranklinOrder
	OrderDate time.Time
	Active bool
	SubTotal float32
	CurrentWeight float32
	Limit float32
}

type FranklinOrder struct {
	Id int
	User string
	Items []FranklinMenuItem
	SubTotal float32
}

type FranklinMenu struct {
	RestaurantName string `json:"restaurant_name"`
	Items []FranklinMenuItem `json:"items"`
}

type FranklinMenuItem struct {
	Id int `json:"id"`// Internal identifier of the Franklin menu item
	Name string `json:"restaurantName"`// Display restaurantName of the Franklin menu item
	Price float32 `json:"price"` // Price of the menu item
	PriceUnitBase float32 `json:"price_unit_base"` // What step unit is the item priced by
	PriceUnit string `json:"price_unit"` // What unit is this priced by (lbs, individual)
	Step float32 `json:"step"`// Step of choices of menu item (per .25 lbs vs. whole items 1)
	Limit bool `json:"limit"`// Does this item contribute to the minimum/maximum order weight limit?
}