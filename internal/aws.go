package internal

import (
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

var (
	Session *session.Session
	DynamoClient *dynamodb.DynamoDB)

func init() {
	/* Initialize the AWS Configs here */
	Session = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	DynamoClient = dynamodb.New(Session)
}
