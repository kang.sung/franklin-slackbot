FROM golang

COPY .  /opt/

RUN go build /opt/main.go
RUN cp /op/main /franklinbot
RUN rm /opt/

ENTRYPOINT ['/franklinbot', 'server', '--port', '8080']
EXPOSE 8080